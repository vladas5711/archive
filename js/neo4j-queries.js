var neo4j_session = require('./config-neo4j');
var fs = require('fs');
var bcrypt = require('bcrypt');
var saltRounds = 10;


function findAdmin(username, password, nextFunc) {
    neo4j_session
        .run('MATCH (admin {username: "' + username + '"}) RETURN admin, id(admin)')
        .then(function(result) {
            var admin = {};
            if (result.records[0] !== undefined) {
                admin.username = result.records[0]._fields[0].properties.username;
                admin.password = result.records[0]._fields[0].properties.password;
                admin.id = result.records[0]._fieldLookup['id(admin)'];
            }
            nextFunc(admin);
        })
        .catch(function(err) {
            console.log(err);
        })
};

function findById(id, nextFunc) {
    neo4j_session
        .run('MATCH (admin) WHERE id(admin)='+id+' RETURN admin')
        .then(function(result) {
            var admin = {};
            if (result.records[0] !== undefined) {
                admin.username = result.records[0]._fields[0].properties.username;
                admin.password = result.records[0]._fields[0].properties.password;
                admin.id = id;
            }
            nextFunc(admin);
        })
        .catch(function(err) {
            console.log(err);
        })
};

function addFund(req, res) {
    var query;
    var data = req.body;
    switch(data.geo) {
        case "Область-Город":
            query = `MERGE (region:Region {name: "`+data.region+`"})
                    MERGE (city:City {name: "`+data.city+`"})-[:BELONGS_TO]-(region)
                    MERGE (fund:Fund {fundCode: "`+data.fundCode+`"})
                    ON CREATE SET fund.fundCreator="`+data.fundCreator+`", fund.fundStart="`+data.fundStart+`", fund.fundEnd="`+data.fundEnd+`", fund.fundCapacity="`+data.fundCapacity+`", fund.fundGeo="`+data.geo+`"
                    MERGE (fund)-[:BELONGS_TO]->(city)`;
            break;
        case "Область-Район-Город":
            query = `MERGE (region:Region {name: "`+data.region+`"})
                    MERGE (district:District {name: "`+data.district+`"})-[:BELONGS_TO]-(region)
                    MERGE (city:City {name: "`+data.city+`"})-[:BELONGS_TO]-(district)
                    MERGE (fund:Fund {fundCode: "`+data.fundCode+`"})
                    ON CREATE SET fund.fundCreator="`+data.fundCreator+`", fund.fundStart="`+data.fundStart+`", fund.fundEnd="`+data.fundEnd+`", fund.fundCapacity="`+data.fundCapacity+`", fund.fundGeo="`+data.geo+`"
                    MERGE (fund)-[:BELONGS_TO]->(city)`;
                    break;
        case "Область-Район-Село":
            query = `MERGE (region:Region {name: "`+data.region+`"})
                    MERGE (district:District {name: "`+data.district+`"})-[:BELONGS_TO]-(region)
                    MERGE (village:Village {name: "`+data.village+`"})-[:BELONGS_TO]-(district)
                    MERGE (fund:Fund {fundCode: "`+data.fundCode+`"})
                    ON CREATE SET fund.fundCreator="`+data.fundCreator+`", fund.fundStart="`+data.fundStart+`", fund.fundEnd="`+data.fundEnd+`", fund.fundCapacity="`+data.fundCapacity+`", fund.fundGeo="`+data.geo+`"
                    MERGE (fund)-[:BELONGS_TO]->(village)`;
            break;
        case "Губерния-Уезд-Волость-Село":
            query = `MERGE (province:Province {name: "`+data.province+`"})
                    MERGE (county:County {name: "`+data.county+`"})-[:BELONGS_TO]-(province)
                    MERGE (parish:Parish {name: "`+data.parish+`"})-[:BELONGS_TO]-(county)
                    MERGE (village:Village {name: "`+data.village+`"})-[:BELONGS_TO]-(parish)
                    MERGE (fund:Fund {fundCode: "`+data.fundCode+`"})
                    ON CREATE SET fund.fundCreator="`+data.fundCreator+`", fund.fundStart="`+data.fundStart+`", fund.fundEnd="`+data.fundEnd+`", fund.fundCapacity="`+data.fundCapacity+`", fund.fundGeo="`+data.geo+`"
                    MERGE (fund)-[:BELONGS_TO]->(village)`;
            break;
        case "Губерния-Уезд-Город":
            query = `MERGE (province:Province {name: "`+data.province+`"})
                    MERGE (county:County {name: "`+data.county+`"})-[:BELONGS_TO]-(province)
                    MERGE (city:City {name: "`+data.city+`"})-[:BELONGS_TO]-(county)
                    MERGE (fund:Fund {fundCode: "`+data.fundCode+`"})
                    ON CREATE SET fund.fundCreator="`+data.fundCreator+`", fund.fundStart="`+data.fundStart+`", fund.fundEnd="`+data.fundEnd+`", fund.fundCapacity="`+data.fundCapacity+`", fund.fundGeo="`+data.geo+`"
                    MERGE (fund)-[:BELONGS_TO]->(city)`;
            break;
      }
    var check = `MATCH (fund:Fund {fundCode: "${data.fundCode}"}) RETURN fund`;
    neo4j_session
        .run(check)
        .then(function(result) {
            if (result.records[0]) {
                res.send('Фонд с таким номером уже существует, запрос отклонен.');
            } else {
                neo4j_session
                    .run(query)
                    .then(function(result) {
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
                    res.send('Добавлен новый фонд!');
            }
        })
        .catch(function(err) {
            console.log(err);
        });
};

function searchFund(req, res) {
    if (req.body.fundCreator == '' && req.body.fundCode == '' && req.body.fundStart == '' && req.body.fundEnd  == '') {
        res.send('<h5><b>Неплохо бы ввести данные для поиска...</h5><b>');
    } else {
        var query;
        var flag = false;
        query = 'match (fund:Fund) WHERE ';
        if (req.body.fundCreator != '') {
                query = query + 'fund.fundCreator =~ "' + req.body.fundCreator + '.*"';
                flag = true;
        }
        if (req.body.fundCode != '') {
            if (!flag) {
                query = query + 'fund.fundCode = "' + req.body.fundCode + '"';
                flag = true;
            } else {
                query = query + ' and fund.fundCode = "' + req.body.fundCode + '"';
            }
        }
        if (req.body.fundStart != '') {
            if (!flag) {
                query = query + 'fund.fundStart = "' + req.body.fundStart + '"';
                flag = true;
            } else {
                query = query + ' and fund.fundStart = "' + req.body.fundStart + '"';
            }
        }
        if (req.body.fundEnd != '') {
            if (!flag) {
                query = query + 'fund.fundEnd = "' + req.body.fundEnd + '"';
                flag = true;
            } else {
                query = query + ' and fund.fundEnd = "' + req.body.fundEnd + '"';
            }
        }
        query = query + 'match (fund)-[:BELONGS_TO]->(place) RETURN fund, place';
        neo4j_session
            .run(query)
            .then(function(result) {
                if (result.records[0]) {
                    sendResults(req, res, result.records);
                } else {
                    res.send('<h5><b>Совпадений не найдено!</h5><b>');
                }
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })
    }
};

function sendResults(req, res, data) {
    var self = this;
    self.table = `
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Код</th>
                        <th>Фондообразователь</th>
                        <th>Начало работы</th>
                        <th>Окончание работы</th>
                        <th>Кол-во дел</th>
                        <th>Нас. пункт</th>
				    </tr>
			    </thead>
			    <tbody>
                `; 
    data.forEach(function(record) {
        var str;
        if (record._fields[1].labels[0] == "City") {
            str = "г. ";
        } else {
            str = "c. ";
        }
		self.table += ` <tr>
            <td width="10%">${record._fields[0].properties.fundCode}</td>
            <td width="50%">${record._fields[0].properties.fundCreator}</td>
            <td width="10%">${record._fields[0].properties.fundStart}</td>
            <td width="10%">${record._fields[0].properties.fundEnd}</td>
            <td width="10%">${record._fields[0].properties.fundCapacity}</td>
            <td width="10%">${str}${record._fields[1].properties.name}</td>
        </tr>` 
    });
    self.table += `
    </tbody>
    </table>`;
    res.send(self.table);
};

function editFund(req, res) {
    if (req.body.fundCreator == '' && req.body.fundStart == '' && req.body.fundEnd  == '' && req.body.fundCapacity  == '') {
    res.send('Ввведите данные для изменения!');
    } else {
        var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                    SET `;
        var flag = false;
        if (req.body.fundCreator != '') {
                query = query + 'fund.fundCreator = "' + req.body.fundCreator + '"';
                flag = true;
        }
        if (req.body.fundStart != '') {
            if (!flag) {
                query = query + 'fund.fundStart = "' + req.body.fundStart + '"';
                flag = true;
            } else {
                query = query + ', fund.fundStart = "' + req.body.fundStart + '"';
            }
        }
        if (req.body.fundEnd != '') {
            if (!flag) {
                query = query + 'fund.fundEnd = "' + req.body.fundEnd + '"';
                flag = true;
            } else {
                query = query + ', fund.fundEnd = "' + req.body.fundEnd + '"';
            }
        }
        if (req.body.fundCapacity != '') {
            if (!flag) {
                query = query + 'fund.fundCapacity = "' + req.body.fundCapacity + '"';
                flag = true;
            } else {
                query = query + ', fund.fundCapacity = "' + req.body.fundCapacity + '"';
            }
        }
        neo4j_session
            .run(query)
            .then(function(result) {
                if (result.summary.updateStatistics._stats.propertiesSet != 0) {
                    res.send('Изменения прошли успешно!');
                } else {
                    res.send('Фонд с таким номером не найден.')
                }
                
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })
    }
    
};

function deleteFund(req, res) {
    var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                OPTIONAL MATCH (inventory:Inventory)-[:BELONGS_TO]->(fund)
                OPTIONAL MATCH (caseN:CaseN)-[:BELONGS_TO]->(inventory)
                OPTIONAL MATCH (card:Card)-[:BELONGS_TO]->(caseN)
                DETACH DELETE card
                DETACH DELETE caseN
                DETACH DELETE inventory
                DETACH DELETE fund`;
    neo4j_session
        .run(query)
        .then(function(result) {
            if (result.summary.updateStatistics._stats.nodesDeleted != 0) {
                res.send('Фонд, и все, что было ему присуще - удалено!');
            } else {
                res.send('Фонда с таким номером нет в базе!')
            }
        })
        .catch(function(err) {
            console.log(err);
        })
};

function renderEditCardTypesPage(req, res) {
    var query = `MATCH (list:List {title: "CardsTypes"})
                MATCH (cardType:CardType)-[:BELONGS_TO]->(list)
                RETURN cardType`;
    neo4j_session
            .run(query)
            .then(function(result) {
                if (result.records[0]) {
                    var data = makeCardTypesTable(req, res, result.records);
                    res.render('edit-card-types', {
                        data: data,
                        title: "Управление типами"
                    });
                } else {
                    res.render('edit-card-types', {
                        title: "Управление типами",
                        data: "<h5><b>На данный момент не задано ни одного шаблона для карточки.</h5><b>"
                    });
                }
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })
};

function makeCardTypesTable(req, res, data) {
    var self = this;
    self.page = `
            <h4 class="mb-3"><b>Существующие типы карточек:</b></h4>
            <table class="table table-striped>
                "<thead>
                    <tr>
                        <th>Тип карточки</th>
                        <th>Поля</th>
				    </tr>
			    </thead>
			    <tbody>
                `;    
    data.forEach(function(record) {
		self.page += ` <tr>
                        <td width="30%">${record._fields[0].properties.name}</td>
                        <td width="70%">`;
        for (var key in record._fields[0].properties) {
            if (key != "name") self.page += `${record._fields[0].properties[key]}, `;
        };
        self.page = self.page.substring(0, self.page.length - 2);
        self.page += `</td>
                      </tr>`; 
    });
    self.page += `
                </tbody>
                </table>`;
    return self.page;
};

function addCardType(req, res) {
    var flag = false;
    var query = `MATCH (list:List {title: "CardsTypes"})
                CREATE (cardType:CardType { name: "${req.body.field_1}", `;
    for (var field in req.body) {
        if (field != 'geo_check' && field != 'date_check' && field != 'fio_check') {
        if (flag) query += `${field}: "${req.body[field]}", `
        else flag = true; }
    }
    if (req.body.geo_check == true) {
        query += `geo_check: "Место события", `;
    }
    if (req.body.date_check == true) {
        query += `date_check: "Дата события", `;
    }
    if (req.body.fio_check == true) {
        query += `fio1_check: "Фамилия", `;
        query += `fio2_check: "Имя", `;
        query += `fio3_check: "Отчество", `;
    }
    query = query.substring(0, query.length - 2);
    query +='})-[:BELONGS_TO]->(list)';
    var check = `MATCH (cardType:CardType {name: "${req.body.field_1}"}) RETURN cardType`;
    neo4j_session
        .run(check)
        .then(function(result) {
            if (result.records[0]) {
                res.send('Тип карточки с таким названием уже существует, запрос отклонен.');
            } else {
                neo4j_session
                    .run(query)
                    .then(function(result) {
                    })
                    .catch(function(err) {
                        res.send("Произошла ошибка...");
                    })
                    res.send('Новый тип карточки добавлен!')
            }
        })
        .catch(function(err) {
            console.log(err);
        });
};

function deleteCardType(req, res) {
    var query = `MATCH (list:List {title: "CardsTypes"})
                MATCH (cardType:CardType {name: "${req.body.name}"})-[:BELONGS_TO]->(list)
                DETACH DELETE cardType`
    neo4j_session
            .run(query)
            .then(function(result) {
                if (result.summary.updateStatistics._stats.nodesDeleted != 0) {
                    res.send('Тип карточки удален!')
                } else {
                    res.send('Тип карточки с таким названием не найден.')
                }
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })
};

function renderAddingCardPage(req, res) {
    var query = `MATCH (list:List {title: "CardsTypes"})
                MATCH (cardType:CardType)-[:BELONGS_TO]->(list)
                RETURN cardType`
    neo4j_session
            .run(query)
            .then(function(result) {
                if (result.records[0]) {
                    var data = makeAddingCardPage(result.records);
                    res.render('adding-card', {
                        title: "Добавление карточки",
                        data: data
                    });
                } else {
                    res.render('adding-card', {
                        title: "Добавление картчоки",
                        data: "<h5><b>В базе не задано типов карточек...</h5></b>"
                    });
                }
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })            

};

function makeAddingCardPage(data) {
    var self = this;
    self.page = `
            <div class="row">
                <div class="col-md-3 mb-3">
                    <label for="cardType"><b>Выберите тип карточки:</b></label>
                    <select class="custom-select d-block w-100" id="cardType" required>
                        `;    
    data.forEach(function(record) {
		self.page += `<option>${record._fields[0].properties.name}</option>`;
    });
    self.page += `
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 mb-3">
                <button class="btn btn-primary btn-lg btn-block" id="btn_render_form">Загрузить форму</button>
            </div>
            <div class="col-md-3 mb-3">
                <button class="btn btn-primary btn-lg btn-block" id="btn_hide_form">Убрать форму</button>
            </div>
        </div>`;
    return self.page;
};

function loadForm(req, res) {
    var query = `MATCH (list:List {title: "CardsTypes"})
                MATCH (cardType:CardType {name: "${req.body.cardType}"})-[:BELONGS_TO]->(list)
                RETURN cardType`
    neo4j_session
            .run(query)
            .then(function(result) {
                var data = makeForm(result.records);
                res.send(data);
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })
};

function makeForm(data) {
    var self = this, temp = data[0]._fields[0].properties;
    self.page = `<form action="/main/adding-card" method="POST" enctype="multipart/form-data">`;   
    self.page += `<div class="row">
                    <div class="col-md-3 mb-3">
                        <input type="text" class="form-control" id="name" name="name" value="${temp.name}" hidden/>
                    </div>
                </div>`; 
    for (var field in temp) {
        if (field != "name") {
        self.page += `<div class="row">
                        <div class="col-md-2 mb-3">
                            <label for="${temp[field]}"><b>${temp[field]}:</b></label>
                        </div>
                        <div class="col-md-5 mb-3">
                            <input type="text" class="form-control" id="${temp[field]}" name="${temp[field]}"/>
                        </div>
                    </div>`;
        }
    };
    self.page += `  
        <div class="row">
            <div class="col-md-3 mb-3">
                <input type="file" name="recfile" id="recfile"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 mb-3">
                <input type="submit" class="btn btn-primary btn-lg btn-block" id="btn_add_card" value="Добавить карточку"/>
            </div>
        </div>
        </form>`;
    return self.page;
};

function addCard(req, res) {
    var data = req.body;
    var query = ``;
    if (data['Место события']) {
        query += `MATCH (fund:Fund {fundCode: "${data['Фонд']}"})
                    MATCH (inventory:Inventory {inventoryCode: "${data['Опись']}"})-[:BELONGS_TO]->(fund)
                    MATCH (caseN:CaseN {caseCode: "${data['Дело']}"})-[:BELONGS_TO]->(inventory)
                    OPTIONAL MATCH (place1 {name: "${data['Место события']}"})<-[:BELONGS_TO]-(fund)
                    OPTIONAL MATCH (place2 {name: "${data['Место события']}"})-[:RENAME_OF]->(place)<-[:BELONGS_TO]-(fund)
                    MERGE (card:Card {type: "${data.name}"})-[:BELONGS_TO]->(caseN)
                    ON CREATE SET `;
        for (var field in data) {
            if(field != 'name') {
                var temps = field.replace(' ', '_');
                query += `card.${temps}="${data[field]}", `;
            };
        };
        query = query.substring(0, query.length - 2);
        query += ` return place1, place2`;
        neo4j_session
            .run(query)
            .then(function(result) {
                if(result.summary.updateStatistics._stats.propertiesSet != 0) {
                    if (result.records[0]._fields[0] || result.records[0]._fields[1]) {
                        var query2 = ``;
                        if (result.records[0]._fields[0]) {
                            query2 += `MATCH (fund:Fund {fundCode: "${data['Фонд']}"})
                                        MATCH (inventory:Inventory {inventoryCode: "${data['Опись']}"})-[:BELONGS_TO]->(fund)
                                        MATCH (caseN:CaseN {caseCode: "${data['Дело']}"})-[:BELONGS_TO]->(inventory)
                                        MATCH (card:Card {type: "${data.name}"})-[:BELONGS_TO]->(caseN)
                                        MATCH (place {name: "${data['Место события']}"})<-[:BELONGS_TO]-(fund)
                                        CREATE (card)-[:WAS_AT]->(place)`;
                        } else {
                            query2 += `MATCH (fund:Fund {fundCode: "${data['Фонд']}"})
                                        MATCH (inventory:Inventory {inventoryCode: "${data['Опись']}"})-[:BELONGS_TO]->(fund)
                                        MATCH (caseN:CaseN {caseCode: "${data['Дело']}"})-[:BELONGS_TO]->(inventory)
                                        MATCH (card:Card {type: "${data.name}"})-[:BELONGS_TO]->(caseN)
                                        MATCH (place {name: "${data['Место события']}"})-[:RENAME_OF]->(place_temp)<-[:BELONGS_TO]-(fund)
                                        CREATE (card)-[:WAS_AT]->(place)`;
                        }
                        neo4j_session
                            .run(query2)
                            .then(function(result) {
                            })
                            .catch(function(err) {
                                console.log(err);
                            })
                        res.render('adding-card', {
                            title: "Добавление карточки",
                            data: `<h5 class="mb-3"><b>Новая карточка была добавлена!</b></h5>
                                    <div class="row">
                                        <div class="col-md-3 mb-3">
                                            <a class="btn btn-primary btn-lg btn-block" href="/main/adding-card">назад</a>
                                        </div>
                                    </div>`
                        });
                    } else {
                        var deleted = `MATCH (fund:Fund {fundCode: "${data['Фонд']}"})
                                MATCH (inventory:Inventory {inventoryCode: "${data['Опись']}"})-[:BELONGS_TO]->(fund)
                                MATCH (caseN:CaseN {caseCode: "${data['Дело']}"})-[:BELONGS_TO]->(inventory)
                                MATCH (card:Card {type: "${data.name}"})-[:BELONGS_TO]->(caseN)
                                DETACH DELETE card`;
                        neo4j_session
                                .run(deleted)
                                .then(function(result) {
                                })
                                .catch(function(err) {
                                    console.log(err);
                                })            
                        res.render('adding-card', {
                            title: "Добавление карточки",
                            data: `<h5 class="mb-3"><b>Невозможно привязать карточкук месту события, такое место события в базе не задано!</b></h5>
                                    <div class="row">
                                        <div class="col-md-3 mb-3">
                                            <a class="btn btn-primary btn-lg btn-block" href="/main/adding-card">назад</a>
                                        </div>
                                    </div>`
                        });
                        fs.unlink(data['image'], function() {});
                    }
                } else {
                    res.render('adding-card', {
                        title: "Добавление карточки",
                        data: `<h5 class="mb-3"><b>Заданой цепочки Фонд-Опись-Дело в базе нет, либо же карточка такого типа уже закреплена за делом.</b></h5>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <a class="btn btn-primary btn-lg btn-block" href="/main/adding-card">назад</a>
                                    </div>
                                </div>`
                    });
                    fs.unlink(data['image'], function() {});
                }
            })
            .catch(function(err) {
                console.log(err);
            })
    } else {
        query += `MATCH (fund:Fund {fundCode: "${data['Фонд']}"})
                MATCH (inventory:Inventory {inventoryCode: "${data['Опись']}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${data['Дело']}"})-[:BELONGS_TO]->(inventory)
                MERGE (card:Card {type: "${data.name}"})-[:BELONGS_TO]->(caseN)
                ON CREATE SET `;
    for (var field in data) {
        if(field != 'name') {
            var temps = field.replace(' ', '_');
            query += `card.${temps}="${data[field]}", `;
        };
    };
    query = query.substring(0, query.length - 2);
    neo4j_session
        .run(query)
        .then(function(result) {
            if(result.summary.updateStatistics._stats.propertiesSet != 0) {
                    res.render('adding-card', {
                        title: "Добавление карточки",
                        data: `<h5 class="mb-3"><b>Новая карточка была добавлена!</b></h5>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <a class="btn btn-primary btn-lg btn-block" href="/main/adding-card">назад</a>
                                    </div>
                                </div>`
                    });
            } else {
                res.render('adding-card', {
                    title: "Добавление карточки",
                    data: `<h5 class="mb-3"><b>Заданой цепочки Фонд-Опись-Дело в базе нет, либо же карточка такого типа уже закреплена за делом.</b></h5>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <a class="btn btn-primary btn-lg btn-block" href="/main/adding-card">назад</a>
                                </div>
                            </div>`
                });
                fs.unlink(data['image'], function() {});
            }
        })
        .catch(function(err) {
            console.log(err);
        })
    }
};

function addInventory(req, res) {
    var query;
    var data = req.body;
    query = `MATCH (fund:Fund {fundCode: "${data.fundCode}"})
            CREATE (inventory:Inventory {inventoryName: "${data.inventoryName}", inventoryCode: "${data.inventoryCode}", inventoryStart: "${data.inventoryStart}", inventoryEnd: "${data.inventoryEnd}"})-[:BELONGS_TO]->(fund)`;
    var check = `MATCH (fund:Fund {fundCode: "${data.fundCode}"})
                OPTIONAL MATCH (inventory:Inventory {inventoryCode: "${data.inventoryCode}"})-[:BELONGS_TO]->(fund)
                RETURN fund, inventory`;
    neo4j_session
        .run(check)
        .then(function(result) {
            if (result.records[0]) {
                if (result.records[0]._fields[1]) {
                    res.send('Опись с таким номером уже существует в данном фонде, запрос отклонен.');
                } else {
                    neo4j_session
                        .run(query)
                        .then(function(result) {
                        })
                        .catch(function(err) {
                            console.log(err);
                        })
                        res.send('Опись добавлена!');
                }
            } else {
                res.send('Фонда с таким номером не существует, запрос отклонен.');
            }
        })
        .catch(function(err) {
            console.log(err);
        });
};

function editInventory(req, res) {
    if (req.body.inventoryName == '' && req.body.inventoryStart == '' && req.body.inventoryEnd  == '') {
        res.send('Ввведите данные для изменения!');
        } else {
            var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                        MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                        SET `;
            var flag = false;
            if (req.body.fundCreator != '') {
                    query = query + 'inventory.inventoryName = "' + req.body.inventoryName + '"';
                    flag = true;
            }
            if (req.body.fundStart != '') {
                if (!flag) {
                    query = query + 'inventory.inventoryStart = "' + req.body.inventoryStart + '"';
                    flag = true;
                } else {
                    query = query + ', inventory.inventoryStart = "' + req.body.inventoryStart + '"';
                }
            }
            if (req.body.fundEnd != '') {
                if (!flag) {
                    query = query + 'inventory.inventoryEnd = "' + req.body.inventoryEnd + '"';
                    flag = true;
                } else {
                    query = query + ', inventory.inventoryEnd = "' + req.body.inventoryEnd + '"';
                }
            }
            var check = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                        MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                        RETURN fund, inventory`;
            neo4j_session
                .run(check)
                .then(function(result) {
                    if (!result.records[0]) {
                        res.send('Такой описи не существует!');
                    } else {
                        neo4j_session
                            .run(query)
                            .then(function(result) {
                            })
                            .catch(function(err) {
                                console.log(err);
                            })
                            res.send('Изменения прошли успешно!');
                    }
                })
                .catch(function(err) {
                    console.log(err);
                });
        }
};

function deleteInventory(req, res) {
    var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                OPTIONAL MATCH (caseN:CaseN)-[:BELONGS_TO]->(inventory)
                OPTIONAL MATCH (card:Card)-[:BELONGS_TO]->(caseN)
                DETACH DELETE card
                DETACH DELETE caseN
                DETACH DELETE inventory`;
    var check = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                RETURN fund, inventory`;
    neo4j_session
        .run(check)
        .then(function(result) {
            if (!result.records[0]) {
                res.send('Такой описи не существует!');
            } else {
                neo4j_session
                    .run(query)
                    .then(function(result) {
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
                    res.send('Опись, и все, что было ей присуще - удалено!');
            }
        })
        .catch(function(err) {
            console.log(err);
        });
};

function addCase(req, res) {
    var query;
    var data = req.body;
    query = `MATCH (fund:Fund {fundCode: "${data.fundCode}"})
            MATCH (inventory:Inventory {inventoryCode: "${data.inventoryCode}"})-[:BELONGS_TO]->(fund)
            CREATE (caseN:CaseN {caseName: "${data.caseName}", caseCode: "${data.caseCode}", caseDate: "${data.caseDate}", caseCapacity: "${data.caseCapacity}"})-[:BELONGS_TO]->(inventory)`;
    var check = `MATCH (fund:Fund {fundCode: "${data.fundCode}"})
                OPTIONAL MATCH (inventory:Inventory {inventoryCode: "${data.inventoryCode}"})-[:BELONGS_TO]->(fund)
                OPTIONAL MATCH (caseN:CaseN {caseCode: "${data.caseCode}"})-[:BELONGS_TO]->(inventory)
                RETURN fund, inventory, caseN`;
    neo4j_session
        .run(check)
        .then(function(result) {
            if (result.records[0]) {
                if (result.records[0]._fields[2]) {
                    res.send('Дело с таким номером уже существует в данных описи и фонде, запрос отклонен.');
                } else {
                    if (result.records[0]._fields[1]) {
                        neo4j_session
                            .run(query)
                            .then(function(result) {
                            })
                            .catch(function(err) {
                                console.log(err);
                            })
                            res.send('Дело добавлено!');
                    } else {
                        res.send('Описи с таким номером не существует в данном фонде, запрос отклонен.');
                    }
                }
            } else {
                res.send('Фонда с таким номером не существует, запрос отклонен.');
            }
        })
        .catch(function(err) {
            console.log(err);
        });
};

function editCase(req, res) {
    if (req.body.caseName == '' && req.body.caseDate == '' && req.body.caseCapacity  == '') {
        res.send('Ввведите данные для изменения!');
        } else {
            var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                        MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                        MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                        SET `;
            var flag = false;
            if (req.body.fundCreator != '') {
                    query = query + 'caseN.caseName = "' + req.body.caseName + '"';
                    flag = true;
            }
            if (req.body.fundStart != '') {
                if (!flag) {
                    query = query + 'caseN.caseDate = "' + req.body.caseDate + '"';
                    flag = true;
                } else {
                    query = query + ', caseN.caseDate = "' + req.body.caseDate + '"';
                }
            }
            if (req.body.fundEnd != '') {
                if (!flag) {
                    query = query + 'caseN.caseCapacity = "' + req.body.caseCapacity + '"';
                    flag = true;
                } else {
                    query = query + ', caseN.caseCapacity = "' + req.body.caseCapacity + '"';
                }
            }
            var check = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                        MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                        MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                        RETURN fund, inventory, caseN`;
            neo4j_session
                .run(check)
                .then(function(result) {
                    if (!result.records[0]) {
                        res.send('Такого дела не существует!');
                    } else {
                        neo4j_session
                            .run(query)
                            .then(function(result) {
                            })
                            .catch(function(err) {
                                console.log(err);
                            })
                            res.send('Изменения прошли успешно!');
                    }
                })
                .catch(function(err) {
                    console.log(err);
                });
        }
};

function deleteCase(req, res) {
    var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                OPTIONAL MATCH (card:Card)-[:BELONGS_TO]->(caseN)
                DETACH DELETE card
                DETACH DELETE caseN`;
    var check = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                RETURN fund, inventory, caseN`;
    neo4j_session
        .run(check)
        .then(function(result) {
            if (!result.records[0]) {
                res.send('Такого дела не существует!');
            } else {
                neo4j_session
                    .run(query)
                    .then(function(result) {
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
                    res.send('Дело, и все, что было ему присуще - удалено!');
            }
        })
        .catch(function(err) {
            console.log(err);
        });
};

function searchInventorys(req, res) {
    if (req.body.inventoryName == '' && req.body.fundCode == '' && req.body.inventoryCode == '' && req.body.inventoryStart  == '' && req.body.inventoryEnd  == '') {
        res.send('<h5><b>Неплохо бы ввести данные для поиска...</h5><b>');
    } else {
        var query = ``;
        var flag = false;
        var tempFund = "";
        if (req.body.fundCode != '') {
            tempFund = `{fundCode: "${req.body.fundCode}"}`;
        }
            if (req.body.inventoryName != '' || req.body.inventoryCode != '' || req.body.inventoryStart  != '' || req.body.inventoryEnd  != '') {
                query += 'match (inventory:Inventory)-[:BELONGS_TO]->(fund) WHERE '; 
                if (req.body.inventoryName != '') {
                        query = query + 'inventory.inventoryName =~ "' + req.body.inventoryName + '.*"';
                        flag = true;
                }
                if (req.body.inventoryCode != '') {
                    if (!flag) {
                        query = query + 'inventory.inventoryCode = "' + req.body.inventoryCode + '"';
                        flag = true;
                    } else {
                        query = query + ' and inventory.inventoryCode = "' + req.body.inventoryCode + '"';
                    }
                }
                if (req.body.inventoryStart != '') {
                    if (!flag) {
                        query = query + 'inventory.inventoryStart = "' + req.body.inventoryStart + '"';
                        flag = true;
                    } else {
                        query = query + ' and inventory.inventoryStart = "' + req.body.inventoryStart + '"';
                    }
                }
                if (req.body.inventoryEnd != '') {
                    if (!flag) {
                        query = query + 'inventory.inventoryEnd = "' + req.body.inventoryEnd + '"';
                        flag = true;
                    } else {
                        query = query + ' and inventory.inventoryEnd = "' + req.body.inventoryEnd + '"';
                    }
                }
            }
        query = query + ` match (inventory)-[:BELONGS_TO]->(fund ${tempFund}) RETURN inventory, fund`;
        neo4j_session
            .run(query)
            .then(function(result) {
                if (result.records[0]) {
                    sendInventorysResults(req, res, result.records);
                } else {
                    res.send('<h5><b>Совпадений не найдено!</h5><b>');
                }
            })
            .catch(function(err) {
                res.send(err);
            })
    }
};

function sendInventorysResults(req, res, data) {
    var temp;
    temp = `
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Код</th>
                        <th>Название описи</th>
                        <th>Год от</th>
                        <th>Год до</th>
                        <th>Код фонда</th>
				    </tr>
			    </thead>
			    <tbody>
                `; 
    data.forEach(function(record) {
		temp += ` <tr>
            <td width="10%">${record._fields[0].properties.inventoryCode}</td>
            <td width="60%">${record._fields[0].properties.inventoryName}</td>
            <td width="10%">${record._fields[0].properties.inventoryStart}</td>
            <td width="10%">${record._fields[0].properties.inventoryEnd}</td>
            <td width="10%">${record._fields[1].properties.fundCode}</td>
        </tr>` 
    });
    temp += `
    </tbody>
    </table>`;
    res.send(temp);
};

function searchCases(req, res) {
    if (req.body.caseName == '' && req.body.fundCode == '' && req.body.inventoryCode == '' && req.body.caseCode  == '' && req.body.caseDate  == '') {
        res.send('<h5><b>Неплохо бы ввести данные для поиска...</h5><b>');
    } else {
        var query = ``;
        var flag = false;
        var tempFund = "", tempInventory = "";
        if (req.body.fundCode != '') {
            tempFund = `{fundCode: "${req.body.fundCode}"}`;
        }
        if (req.body.inventoryCode != '') {
            tempInventory = `{inventoryCode: "${req.body.inventoryCode}"}`;
        }
                if (req.body.caseCode != '' || req.body.caseName != '' || req.body.caseDate != '') {
                    query += `match (caseN:CaseN)-[:BELONGS_TO]->(inventory) WHERE `;
                    if (req.body.caseName != '') {
                        query = query + 'caseN.caseName =~ "' + req.body.caseName + '.*"';
                        flag = true;
                    }
                    if (req.body.caseCode != '') {
                        if (!flag) {
                            query = query + 'caseN.caseCode = "' + req.body.caseCode + '"';
                            flag = true;
                        } else {
                            query = query + ' and caseN.caseCode = "' + req.body.caseCode + '"';
                        }
                    }
                    if (req.body.caseDate != '') {
                        if (!flag) {
                            query = query + 'caseN.caseDate = "' + req.body.caseDate + '"';
                            flag = true;
                        } else {
                            query = query + ' and caseN.caseDate = "' + req.body.caseDate + '"';
                        }
                    }
                }
        query = query + ` match (caseN)-[:BELONGS_TO]->(inventory:Inventory ${tempInventory})-[:BELONGS_TO]->(fund:Fund ${tempFund}) RETURN caseN, inventory, fund`;
        neo4j_session
            .run(query)
            .then(function(result) {
                if (result.records[0]) {
                    sendCasesResults(req, res, result.records);
                } else {
                    res.send('<h5><b>Совпадений не найдено!</h5><b>');
                }
            })
            .catch(function(err) {
                res.send(err);
            })
    }
};

function sendCasesResults(req, res, data) {
    var temp;
    temp = `
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Код</th>
                        <th>Название дела</th>
                        <th>Дата</th>
                        <th>Кол-во листов</th>
                        <th>Код описи</th>
                        <th>Код фонда</th>
				    </tr>
			    </thead>
			    <tbody>
                `; 
    data.forEach(function(record) {
		temp += ` <tr>
            <td width="10%">${record._fields[0].properties.caseCode}</td>
            <td width="50%">${record._fields[0].properties.caseName}</td>
            <td width="10%">${record._fields[0].properties.caseDate}</td>
            <td width="10%">${record._fields[0].properties.caseCapacity}</td>
            <td width="10%">${record._fields[1].properties.inventoryCode}</td>
            <td width="10%">${record._fields[2].properties.fundCode}</td>
        </tr>` 
    });
    temp += `
    </tbody>
    </table>`;
    res.send(temp);
};

function addRename(req, res) {
    if (req.body.realType == 'none') {
        res.send('Вы не указали тип места для текущего названия!');
        return;
    }
    var query, check;
    if (req.body.grandparentType != 'none' && req.body.grandparentName != 'none') {
        switch (req.body.grandparentType) {
            case 'Область': 
                check = `MATCH (grandparent:Region {name: "${req.body.grandparentName}"}) `
            break;
            case 'Губерния': 
                check = `MATCH (grandparent:Province {name: "${req.body.grandparentName}"}) `
            break;
            case 'Уезд':
                check = `MATCH (grandparent:County {name: "${req.body.grandparentName}"}) `
            break;
        }
        switch (req.body.parentType) {
            case 'Район': 
                check += `MATCH (parent:District {name: "${req.body.parentName}"})-[:BELONGS_TO]->(grandparent) `
            break;
            case 'Уезд': 
                check += `MATCH (parent:County {name: "${req.body.parentName}"})-[:BELONGS_TO]->(grandparent) `
            break;
            case 'Волость':
                check += `MATCH (parent:Parish {name: "${req.body.parentName}"})-[:BELONGS_TO]->(grandparent) `
            break;
            default:
                res.send('Неверно задано прямое подчинение!');
                return;
        } 
        switch (req.body.realType) {
            case 'Село': 
                check += `MATCH (child:Village {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
            break;
            case 'Город': 
                check += `MATCH (child:City {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
            break;
            case 'Волость':
                check += `MATCH (child:Parish {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
            break;
            default:
                res.send('Неверно задано тип места для переименования!');
                return;
        }
        query = check;
        query += `CREATE (rename:Rename {name: "${req.body.oldName}"})-[:RENAME_OF {yearStart: "${req.body.yearStart}", yearEnd: "${req.body.yearEnd}"}]->(child) 
                  CREATE (rename)-[:BELONGS_TO]->(parent)`;
    } else {
        if (req.body.parentType != 'none' && req.body.parentName != 'none') {
            switch (req.body.parentType) {
                case 'Район': 
                    check = `MATCH (parent:District {name: "${req.body.parentName}"}) `
                break;
                case 'Уезд': 
                    check = `MATCH (parent:County {name: "${req.body.parentName}"}) `
                break;
                case 'Волость':
                    check = `MATCH (parent:Parish {name: "${req.body.parentName}"}) `
                break;
                case 'Область': 
                check = `MATCH (parent:Region {name: "${req.body.parentName}"}) `
                break;
                case 'Губерния': 
                    check = `MATCH (parent:Province {name: "${req.body.parentName}"}) `
                break;
            }
            switch (req.body.realType) {
                case 'Село': 
                    check += `MATCH (child:Village {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Город': 
                    check += `MATCH (child:City {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Волость':
                    check += `MATCH (child:Parish {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Уезд':
                    check += `MATCH (child:County {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Район':
                    check += `MATCH (child:District {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                default:
                    res.send('Неверно задано тип места для переименования!');
                    return;
            }
            query = check;
            query += `CREATE (rename:Rename {name: "${req.body.oldName}"})-[:RENAME_OF {yearStart: "${req.body.yearStart}", yearEnd: "${req.body.yearEnd}"}]->(child) 
                    CREATE (rename)-[:BELONGS_TO]->(parent)`;
        } else {
            switch (req.body.realType) {
                case 'Село': 
                    check = `MATCH (child:Village {name: "${req.body.realName}"}) `
                break;
                case 'Город': 
                    check = `MATCH (child:City {name: "${req.body.realName}"}) `
                break;
                case 'Волость':
                    check = `MATCH (child:Parish {name: "${req.body.realName}"}) `
                break;
                case 'Уезд':
                    check = `MATCH (child:County {name: "${req.body.realName}"}) `
                break;
                case 'Район':
                    check = `MATCH (child:District {name: "${req.body.realName}"}) `
                break;
                case 'Губерния':
                    check = `MATCH (child:Province {name: "${req.body.realName}"}) `
                break;
                case 'Область':
                    check = `MATCH (child:Region {name: "${req.body.realName}"}) `
                break;
            }
            query = check;
            query += `CREATE (rename:Rename {name: "${req.body.oldName}"})-[:RENAME_OF {yearStart: "${req.body.yearStart}", yearEnd: "${req.body.yearEnd}"}]->(child)`;
        }
    }
    neo4j_session
            .run(query)
            .then(function(result) {
                if(result.summary.updateStatistics._stats.propertiesSet != 0) {
                    res.send('Переименование успешно добавлено!');
                } else {
                    res.send('Такого места нет в базе!');
                }
            })
            .catch(function(err) {
                res.send(err);
            })
};

function addAccount(req, res) {
    var hash_pass;
    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
        hash_pass = hash;
    });
    var query = `MATCH (list:List {title: "AdminsList"})
                MERGE (admin:Admin {username: "${req.body.username}"})
                ON CREATE SET admin.name = "${req.body.name}", admin.password = "${hash_pass}" `;
    neo4j_session
        .run(query)
        .then(function(result) {
            if (result.summary.updateStatistics._stats.nodesCreated != 0) {
                res.send('Аккаунт успешно создан!');
            } else {
                res.send('Аккаунт с таким логином уже существует');
            }
        })
        .catch(function(err) {
            console.log(err);
        });
}

function deleteAccount(req, res) {
var query = `MATCH (list:List {title: "AdminsList"})
            MATCH (admin:Admin {username: "${req.body.username}"})-[:BELONGS_TO]->(list)
            DETACH DELETE admin`;
    neo4j_session
        .run(query)
        .then(function(result) {
            if (result.summary.updateStatistics._stats.nodesDeleted != 0) {
                res.send("Аккаунт успешно удален!");
            } else {
                res.send("Аккаунта с таким логином нет в базе!");
            }
        })
        .catch(function(err) {
            console.log(err);
        });
}

function renderAccountPage(req, res) {
    var query = `MATCH (list:List {title: "AdminsList"})
                MATCH (admin:Admin)-[:BELONGS_TO]->(list)
                RETURN admin`;
    neo4j_session
            .run(query)
            .then(function(result) {
                var table = makeAccountPageTable(result.records);
                res.render('account', {
                    title: "Аккаунты",
                    data: table
                });
            })
            .catch(function(err) {
                res.send(err);
            })
};

function makeAccountPageTable(data) {
    var temp;
    temp = `
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Логин</th>
                        <th>Пароль (хэш-значение)</th>
				    </tr>
			    </thead>
			    <tbody>
                `; 
    data.forEach(function(record) {
		temp += ` <tr>
            <td width="50%">${record._fields[0].properties.name}</td>
            <td width="25%">${record._fields[0].properties.username}</td>
            <td width="25%">${record._fields[0].properties.password}</td>
        </tr>` 
    });
    temp += `
    </tbody>
    </table>`;
    return temp;
};

function searchCards(req, res) {
    var query = ``;
    var data = req.body;
    var flag = true;
    for (var key in data) {
        if (key != 'placeType') {
            if (data[key] != '') flag = false;
        }
    }
    if (flag) {
        res.send("<h5><b>Неплохо бы ввести данные для поиска...</h5><b>");
        return;
    }
    if (data.placeType != 'none') {
        if (data.place != '') {
            switch (data.placeType) {
                case 'Село': 
                    query += `MATCH (place_village:Village {name: "${data.place}"}) 
                            MATCH (fund:Fund)-[:BELONGS_TO]->(place_village) WHERE fund.fundCreator =~ "${data.fundName}.*" and fund.fundCode =~ "${data.fundCode}.*"
                            MATCH (inventory:Inventory)-[:BELONGS_TO]->(fund) WHERE inventory.inventoryCode =~ "${data.inventoryCode}.*"
                            MATCH (caseN:CaseN)-[:BELONGS_TO]->(inventory) WHERE caseN.caseName =~ "${data.caseName}.*" and caseN.caseCode =~ "${data.caseCode}.*"
                            MATCH (card:Card)-[:BELONGS_TO]->(caseN) WHERE card.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card['Имя'] =~ "${data.name}.*" and card['Фамилия'] =~ "${data.surname}.*" and card['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card `;
                break;
                case 'Город': 
                    query += `MATCH (place_city:City {name: "${data.place}"}) 
                            MATCH (fund:Fund)-[:BELONGS_TO]->(place_city) WHERE fund.fundCreator =~ "${data.fundName}.*" and fund.fundCode =~ "${data.fundCode}.*"
                            MATCH (inventory:Inventory)-[:BELONGS_TO]->(fund) WHERE inventory.inventoryCode =~ "${data.inventoryCode}.*"
                            MATCH (caseN:CaseN)-[:BELONGS_TO]->(inventory) WHERE caseN.caseName =~ "${data.caseName}.*" and caseN.caseCode =~ "${data.caseCode}.*"
                            MATCH (card:Card)-[:BELONGS_TO]->(caseN) WHERE card.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card['Имя'] =~ "${data.name}.*" and card['Фамилия'] =~ "${data.surname}.*" and card['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card `;
                break;
                case 'Волость':
                    query += `MATCH (parish:Parish {name: "${data.place}"}) 
                            MATCH (place_village:Village)-[:BELONGS_TO]->(parish)
                            MATCH (fund:Fund)-[:BELONGS_TO]->(place_village) WHERE fund.fundCreator =~ "${data.fundName}.*" and fund.fundCode =~ "${data.fundCode}.*"
                            MATCH (inventory:Inventory)-[:BELONGS_TO]->(fund) WHERE inventory.inventoryCode =~ "${data.inventoryCode}.*"
                            MATCH (caseN:CaseN)-[:BELONGS_TO]->(inventory) WHERE caseN.caseName =~ "${data.caseName}.*" and caseN.caseCode =~ "${data.caseCode}.*" 
                            MATCH (card:Card)-[:BELONGS_TO]->(caseN) WHERE card.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card['Имя'] =~ "${data.name}.*" and card['Фамилия'] =~ "${data.surname}.*" and card['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card `;
                break;
                case 'Уезд':
                    query += `MATCH (county:County {name: "${data.place}"}) 
                            OPTIONAL MATCH (parish:Parish)-[:BELONGS_TO]->(county)
                            OPTIONAL MATCH (place_village:Village)-[:BELONGS_TO]->(parish)
                            OPTIONAL MATCH (place_city:City)-[:BELONGS_TO]->(county)
                            OPTIONAL MATCH (fund_city:Fund)-[:BELONGS_TO]->(place_city) WHERE fund_city.fundCreator =~ "${data.fundName}.*" and fund_city.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (fund_village:Fund)-[:BELONGS_TO]->(place_village) WHERE fund_village.fundCreator =~ "${data.fundName}.*" and fund_village.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (inventory_city:Inventory)-[:BELONGS_TO]->(fund_city) WHERE inventory_city.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (inventory_village:Inventory)-[:BELONGS_TO]->(fund_village) WHERE inventory_village.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (caseN_city:CaseN)-[:BELONGS_TO]->(inventory_city) WHERE caseN_city.caseName =~ "${data.caseName}.*" and caseN_city.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (caseN_village:CaseN)-[:BELONGS_TO]->(inventory_village) WHERE caseN_village.caseName =~ "${data.caseName}.*" and caseN_village.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (card_village:Card)-[:BELONGS_TO]->(caseN_village) WHERE card_village.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_village['Имя'] =~ "${data.name}.*" and card_village['Фамилия'] =~ "${data.surname}.*" and card_village['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_village['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `OPTIONAL MATCH (card_city:Card)-[:BELONGS_TO]->(caseN_city) WHERE card_city.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_city['Имя'] =~ "${data.name}.*" and card_city['Фамилия'] =~ "${data.surname}.*" and card_city['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_city['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card_village, card_city `;
                break;
                case 'Район':
                    query += `MATCH (district:District {name: "${data.place}"})
                            OPTIONAL MATCH (place_village:Village)-[:BELONGS_TO]->(district)
                            OPTIONAL MATCH (place_city:City)-[:BELONGS_TO]->(district) 
                            OPTIONAL MATCH (fund_city:Fund)-[:BELONGS_TO]->(place_city) WHERE fund_city.fundCreator =~ "${data.fundName}.*" and fund_city.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (fund_village:Fund)-[:BELONGS_TO]->(place_village) WHERE fund_village.fundCreator =~ "${data.fundName}.*" and fund_village.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (inventory_city:Inventory)-[:BELONGS_TO]->(fund_city)  WHERE inventory_city.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (inventory_village:Inventory)-[:BELONGS_TO]->(fund_village) WHERE inventory_village.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (caseN_city:CaseN)-[:BELONGS_TO]->(inventory_city) WHERE caseN_city.caseName =~ "${data.caseName}.*" and caseN_city.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (caseN_village:CaseN)-[:BELONGS_TO]->(inventory_village) WHERE caseN_village.caseName =~ "${data.caseName}.*" and caseN_village.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (card_village:Card)-[:BELONGS_TO]->(caseN_village) WHERE card_village.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_village['Имя'] =~ "${data.name}.*" and card_village['Фамилия'] =~ "${data.surname}.*" and card_village['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_village['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `OPTIONAL MATCH (card_city:Card)-[:BELONGS_TO]->(caseN_city) WHERE card_city.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_city['Имя'] =~ "${data.name}.*" and card_city['Фамилия'] =~ "${data.surname}.*" and card_city['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_city['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card_village, card_city `;
                break;
                case 'Губерния':
                    query += `MATCH (province:Province {name: "${data.place}"}) 
                            MATCH (county:County)-[:BELONGS_TO]->(province)
                            OPTIONAL MATCH (parish:Parish)-[:BELONGS_TO]->(county)
                            OPTIONAL MATCH (place_village:Village)-[:BELONGS_TO]->(parish)
                            OPTIONAL MATCH (place_city:City)-[:BELONGS_TO]->(county) 
                            OPTIONAL MATCH (fund_city:Fund)-[:BELONGS_TO]->(place_city) WHERE fund_city.fundCreator =~ "${data.fundName}.*" and fund_city.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (fund_village:Fund)-[:BELONGS_TO]->(place_village) WHERE fund_village.fundCreator =~ "${data.fundName}.*" and fund_village.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (inventory_city:Inventory)-[:BELONGS_TO]->(fund_city) WHERE inventory_city.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (inventory_village:Inventory)-[:BELONGS_TO]->(fund_village) WHERE inventory_village.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (caseN_city:CaseN)-[:BELONGS_TO]->(inventory_city) WHERE caseN_city.caseName =~ "${data.caseName}.*" and caseN_city.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (caseN_village:CaseN)-[:BELONGS_TO]->(inventory_village) WHERE caseN_village.caseName =~ "${data.caseName}.*" and caseN_village.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (card_village:Card)-[:BELONGS_TO]->(caseN_village) WHERE card_village.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_village['Имя'] =~ "${data.name}.*" and card_village['Фамилия'] =~ "${data.surname}.*" and card_village['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_village['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `OPTIONAL MATCH (card_city:Card)-[:BELONGS_TO]->(caseN_city) WHERE card_city.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_city['Имя'] =~ "${data.name}.*" and card_city['Фамилия'] =~ "${data.surname}.*" and card_city['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_city['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card_village, card_city `;
                break;
                case 'Область':
                    query += `MATCH (region:Region {name: "${data.place}"}) 
                            OPTIONAL MATCH (district:District)-[:BELONGS_TO]->(region)
                            OPTIONAL MATCH (place_village:Village)-[:BELONGS_TO]->(district)
                            OPTIONAL MATCH (place_city1:City)-[:BELONGS_TO]->(district) 
                            OPTIONAL MATCH (place_city2:City)-[:BELONGS_TO]->(region) 
                            OPTIONAL MATCH (fund_city1:Fund)-[:BELONGS_TO]->(place_city1) WHERE fund_city1.fundCreator =~ "${data.fundName}.*" and fund_city1.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (fund_city2:Fund)-[:BELONGS_TO]->(place_city2) WHERE fund_city2.fundCreator =~ "${data.fundName}.*" and fund_city2.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (fund_village:Fund)-[:BELONGS_TO]->(place_village) WHERE fund_village.fundCreator =~ "${data.fundName}.*" and fund_village.fundCode =~ "${data.fundCode}.*"
                            OPTIONAL MATCH (inventory_city1:Inventory)-[:BELONGS_TO]->(fund_city1) WHERE inventory_city1.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (inventory_city2:Inventory)-[:BELONGS_TO]->(fund_city2) WHERE inventory_city2.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (inventory_village:Inventory)-[:BELONGS_TO]->(fund_village) WHERE inventory_village.inventoryCode =~ "${data.inventoryCode}.*"
                            OPTIONAL MATCH (caseN_city1:CaseN)-[:BELONGS_TO]->(inventory_city1) WHERE caseN_city1.caseName =~ "${data.caseName}.*" and caseN_city1.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (caseN_city2:CaseN)-[:BELONGS_TO]->(inventory_city2) WHERE caseN_city2.caseName =~ "${data.caseName}.*" and caseN_city2.caseCode =~ "${data.caseCode}.*"
                            OPTIONAL MATCH (caseN_village:CaseN)-[:BELONGS_TO]->(inventory_village) WHERE caseN_village.caseName =~ "${data.caseName}.*" and caseN_village.caseCode =~ "${data.caseCode}.*" 
                            OPTIONAL MATCH (card_village:Card)-[:BELONGS_TO]->(caseN_village) WHERE card_village.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_village['Имя'] =~ "${data.name}.*" and card_village['Фамилия'] =~ "${data.surname}.*" and card_village['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_village['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `OPTIONAL MATCH (card_city1:Card)-[:BELONGS_TO]->(caseN_city1) WHERE card_city1.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_city1['Имя'] =~ "${data.name}.*" and card_city1['Фамилия'] =~ "${data.surname}.*" and card_city1['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_city1['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `OPTIONAL MATCH (card_city2:Card)-[:BELONGS_TO]->(caseN_city2) WHERE card_city2.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card_city2['Имя'] =~ "${data.name}.*" and card_city2['Фамилия'] =~ "${data.surname}.*" and card_city2['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card_city2['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card_village, card_city1, card_city2`;
                break;
            }
        } else {
            res.send("<h5><b>Вы указали тип места, но не задали его название!</h5><b>");
        }
    } else {
        query += `MATCH (fund:Fund) WHERE fund.fundCreator =~ "${data.fundName}.*" and fund.fundCode =~ "${data.fundCode}.*"
                MATCH (inventory:Inventory)-[:BELONGS_TO]->(fund)  WHERE inventory.inventoryCode =~ "${data.inventoryCode}.*"
                MATCH (caseN:CaseN)-[:BELONGS_TO]->(inventory) WHERE caseN.caseName =~ "${data.caseName}.*" and caseN.caseCode =~ "${data.caseCode}.*"
                MATCH (card:Card)-[:BELONGS_TO]->(caseN) WHERE card.type =~ "${data.type}.*" `; 
                    if (data.name || data.surname || data.partonymic) {
                        query += `and card['Имя'] =~ "${data.name}.*" and card['Фамилия'] =~ "${data.surname}.*" and card['Отчество'] =~ "${data.partonymic}.*" `
                    }
                    if (data.date) {
                        query += `and card['Дата_события'] =~ ".*${data.date}.*" `
                    }
                    query += `RETURN card `;
    }
    if (query != '') {
        neo4j_session
                .run(query)
                .then(function(result) {
                    makeCardsTable(req, res, result.records);
                })
                .catch(function(err) {
                    res.send(err);
                })
    }
};

function makeCardsTable(req, res, data) {
    var temp;
    var flag = true;
    temp = `
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Фонд</th>
                        <th>Опись</th>
                        <th>Дело</th>
                        <th>Тип карточки</th>
                        <th>Перечень полей</th> 
				    </tr>
			    </thead>
			    <tbody>
                `; 
    data.forEach(function(record) {
        record._fields.forEach(function(rec) {
            if (rec !== null) {
                flag = false;
                temp += ` <tr>
                    <td width="10%">${rec.properties['Фонд']}</td>
                    <td width="10%">${rec.properties['Опись']}</td>
                    <td width="10%">${rec.properties['Дело']}</td>
                    <td width="10%">${rec.properties.type}</td>
                    <td width="50%">`
                    for (var key in rec.properties) {
                        if (key != 'image' && key != 'type' && key != 'Фонд' && key != 'Опись' && key != 'Дело') {
                            var temps = key.replace('_', ' ');
                            temp += `<b>${temps}</b>: ${rec.properties[key]}, `;
                        }
                    }
                temp = temp.substring(0, temp.length - 2);
                temp += `&nbsp&nbsp<button id="${rec.properties['Фонд']}_${rec.properties['Опись']}_${rec.properties['Дело']}_${rec.properties.type}">Изображеие</button>`;
                temp += `</td>
                </tr>`;
            }
        }) 
    });
    temp += `
    </tbody>
    </table>`;
    if (flag) {
        res.send('<h5><b>Совпадений не найдено!</b></h5>');
    } else {
        res.send(temp);
    }
};

function renderCardPage(req, res) {
    var card = req.params.id.split('_');
    var query = `MATCH (fund:Fund {fundCode: "${card[0]}"})
                MATCH (inventory:Inventory {inventoryCode: "${card[1]}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${card[2]}"})-[:BELONGS_TO]->(inventory)
                MATCH (card:Card {type: "${card[3]}"})-[:BELONGS_TO]->(caseN)
                RETURN card`;
    neo4j_session
            .run(query)
            .then(function(result) {
                makeCardPage(req, res, result.records[0]._fields[0].properties);
            })
            .catch(function(err) {
                res.send(err);
            })
};

function makeCardPage(req, res, data) {
    var page = `<div class="col-md-12 mb-3"><label><b>Тип карточки: ${data.type}</b></label></div>`;
    for (var field in data) {
        if (field != 'image' && field != 'type') {
            page += `<div class="col-md-12 mb-3"><label><b>${field}: ${data[field]}</b></label></div>`;
        }
    }
    if (data.image != 'no_image') {
        page += `<img src="/${data.image}"></img>`;
    } else {
        page += `<div class="col-md-12 mb-3"><label><b>Изображение: не задано</b></label></div>`;
    }
    res.send(page);
};

function loadCardForm(req, res) {
    var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                MATCH (card:Card {type: "${req.body.type}"})-[:BELONGS_TO]->(caseN)
                RETURN card`;
        neo4j_session
            .run(query)
            .then(function(result) {
                if (result.records[0]) {
                    makeCardForm(req, res, result.records[0]._fields[0].properties);
                } else {
                    res.send('<h5><b>Такой карточки нет в базе!</b></h5>')
                 }
            })
            .catch(function(err) {
                res.send("Произошла ошибка...");
            })    
};

function makeCardForm(req, res, data) {
    var temp = `<form action="/main/edit-cards-result" method="POST" enctype="multipart/form-data">
                <input type="text" class="form-control" name="fundCode" id="fundCode" value="${data['Фонд']}" hidden/>
                <input type="text" class="form-control" name="inventoryCode" id="inventoryCode" value="${data['Опись']}" hidden/>
                <input type="text" class="form-control" name="caseCode" id="caseCode" value="${data['Дело']}" hidden/>
                <input type="text" class="form-control" name="type" id="type" value="${data.type}" hidden/>`;
    for (var field in data) {
        if (field != 'Фонд' && field != 'Опись' && field != 'Дело' && field != 'type' && field != 'image'  && field != 'Место_события') {
            temp += `<div class="row">
                                <div class="col-md-2 mb-3">
                                    <h6><b>${field}:</b></h6>
                                </div>
                                <div class="col-md-5 mb-3">
                                    <input type="text" class="form-control" id="${field}">
                                </div>
                            </div>`;
        }
    }
    temp += `<div class="row">
            <div class="col-md-3 mb-3">
                <input type="file" name="recfile" id="recfile"/>
            </div>
        </div>
    <div class="row">
    <div class="col-md-3 mb-3">
        <button type="submit" class="btn btn-primary btn-lg btn-block" id="btn_edit_card">Редактировать</button>
    </div>
  </div>
  </form>`;
  res.send(temp);
};

function editCard(req, res) {
    var flag = false;
    var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                MATCH (card:Card {type: "${req.body.type}"})-[:BELONGS_TO]->(caseN)
                SET `;
    for (var key in req.body) {
        if (key != 'fundCode' && key != 'inventoryCode' && key != 'caseCode' && key != 'type' && key != 'Место_события') {
            if (req.body[key] != '' && req.body[key] != 'no_image') {
                flag = true;
                var temps = key.replace(' ', '_');
                query += `card.${temps} = "${req.body[key]}", `
            }
        }
    }
    query = query.substring(0, query.length - 2);
    if (flag) {
        neo4j_session
                .run(query)
                .then(function(result) {
                    if (result.summary.updateStatistics._stats.propertiesSet != 0) {
                        res.render('edit-cards-result', {
                            title: "Результат",
                            data: `<h5 class="mb-3"><b>Операция успешна!</b></h5>
                                    <div class="row">
                                        <div class="col-md-3 mb-3">
                                            <a class="btn btn-primary btn-lg btn-block" href="/main/edit-cards">назад</a>
                                        </div>
                                    </div>`
                        });
                    } else {
                        res.render('edit-cards-result', {
                            title: "Результат",
                            data: `<h5 class="mb-3"><b>Карточка не найдена!</b></h5>
                                    <div class="row">
                                        <div class="col-md-3 mb-3">
                                            <a class="btn btn-primary btn-lg btn-block" href="/main/edit-cards">назад</a>
                                        </div>
                                    </div>`
                        });
                        if (req.body.image != 'no_image') {
                            fs.unlink(req.body.image, function() {});
                        }
                    }
                })
                .catch(function(err) {
                    res.send(err);
                })
    } else {
        res.render('edit-cards-result', {
            title: "Результат",
            data: `<h5 class="mb-3"><b>Вы не задали полей для изменения, ничего не произошло...</b></h5>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <a class="btn btn-primary btn-lg btn-block" href="/main/edit-cards">назад</a>
                        </div>
                    </div>`
        });
    }

};

function deleteCard(req, res) {
    var query = `MATCH (fund:Fund {fundCode: "${req.body.fundCode}"})
                MATCH (inventory:Inventory {inventoryCode: "${req.body.inventoryCode}"})-[:BELONGS_TO]->(fund)
                MATCH (caseN:CaseN {caseCode: "${req.body.caseCode}"})-[:BELONGS_TO]->(inventory)
                MATCH (card:Card {type: "${req.body.type}"})-[:BELONGS_TO]->(caseN)
                DETACH DELETE card`;
    neo4j_session
        .run(query)
        .then(function(result) {
            if (result.summary.updateStatistics._stats.nodesDeleted != 0) {
                res.send('Карточка была удалена!');
            } else {
                res.send('Такой карточки нет в базе!')
            }
        })
        .catch(function(err) {
            console.log(err);
        })
};

function rename(req, res) {
    if (req.body.realType == 'none') {
        res.send('Вы не указали тип места для текущего названия!');
        return;
    }
    var query, check;
    if (req.body.grandparentType != 'none' && req.body.grandparentName != 'none') {
        switch (req.body.grandparentType) {
            case 'Область': 
                check = `MATCH (grandparent:Region {name: "${req.body.grandparentName}"}) `
            break;
            case 'Губерния': 
                check = `MATCH (grandparent:Province {name: "${req.body.grandparentName}"}) `
            break;
            case 'Уезд':
                check = `MATCH (grandparent:County {name: "${req.body.grandparentName}"}) `
            break;
        }
        switch (req.body.parentType) {
            case 'Район': 
                check += `MATCH (parent:District {name: "${req.body.parentName}"})-[:BELONGS_TO]->(grandparent) `
            break;
            case 'Уезд': 
                check += `MATCH (parent:County {name: "${req.body.parentName}"})-[:BELONGS_TO]->(grandparent) `
            break;
            case 'Волость':
                check += `MATCH (parent:Parish {name: "${req.body.parentName}"})-[:BELONGS_TO]->(grandparent) `
            break;
            default:
                res.send('Неверно задано прямое подчинение!');
                return;
        } 
        switch (req.body.realType) {
            case 'Село': 
                check += `MATCH (child:Village {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
            break;
            case 'Город': 
                check += `MATCH (child:City {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
            break;
            case 'Волость':
                check += `MATCH (child:Parish {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
            break;
            default:
                res.send('Неверно задано тип места для переименования!');
                return;
        }
        query = check;
        query += `SET child.name = "${req.body.newName}"`;
    } else {
        if (req.body.parentType != 'none' && req.body.parentName != 'none') {
            switch (req.body.parentType) {
                case 'Район': 
                    check = `MATCH (parent:District {name: "${req.body.parentName}"}) `
                break;
                case 'Уезд': 
                    check = `MATCH (parent:County {name: "${req.body.parentName}"}) `
                break;
                case 'Волость':
                    check = `MATCH (parent:Parish {name: "${req.body.parentName}"}) `
                break;
                case 'Область': 
                check = `MATCH (parent:Region {name: "${req.body.parentName}"}) `
                break;
                case 'Губерния': 
                    check = `MATCH (parent:Province {name: "${req.body.parentName}"}) `
                break;
            }
            switch (req.body.realType) {
                case 'Село': 
                    check += `MATCH (child:Village {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Город': 
                    check += `MATCH (child:City {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Волость':
                    check += `MATCH (child:Parish {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Уезд':
                    check += `MATCH (child:County {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                case 'Район':
                    check += `MATCH (child:District {name: "${req.body.realName}"})-[:BELONGS_TO]->(parent) `
                break;
                default:
                    res.send('Неверно задано тип места для переименования!');
                    return;
            }
            query = check;
            query += `SET child.name = "${req.body.newName}"`;
        } else {
            switch (req.body.realType) {
                case 'Село': 
                    check = `MATCH (child:Village {name: "${req.body.realName}"}) `
                break;
                case 'Город': 
                    check = `MATCH (child:City {name: "${req.body.realName}"}) `
                break;
                case 'Волость':
                    check = `MATCH (child:Parish {name: "${req.body.realName}"}) `
                break;
                case 'Уезд':
                    check = `MATCH (child:County {name: "${req.body.realName}"}) `
                break;
                case 'Район':
                    check = `MATCH (child:District {name: "${req.body.realName}"}) `
                break;
                case 'Губерния':
                    check = `MATCH (child:Province {name: "${req.body.realName}"}) `
                break;
                case 'Область':
                    check = `MATCH (child:Region {name: "${req.body.realName}"}) `
                break;
            }
            query = check;
            query += `SET child.name = "${req.body.newName}"`;
        }
    }
    neo4j_session
            .run(query)
            .then(function(result) {
                if(result.summary.updateStatistics._stats.propertiesSet != 0) {
                    res.send('Операция успешна!');
                } else {
                    res.send('Такого места нет в базе!');
                }
            })
            .catch(function(err) {
                res.send(err);
            })
};

module.exports = {
    findAdmin: findAdmin,
    findById: findById,
    addFund: addFund,
    searchFund: searchFund,
    editFund: editFund,
    deleteFund: deleteFund,
    renderEditCardTypesPage: renderEditCardTypesPage,
    addCardType: addCardType,
    deleteCardType: deleteCardType, 
    renderAddingCardPage: renderAddingCardPage,
    addCard: addCard,
    loadForm: loadForm,
    addInventory: addInventory,
    editInventory: editInventory,
    deleteInventory: deleteInventory,
    addCase: addCase,
    editCase: editCase,
    deleteCase: deleteCase,
    searchInventorys: searchInventorys,
    searchCases: searchCases,
    addRename: addRename,
    addAccount: addAccount,
    deleteAccount: deleteAccount,
    renderAccountPage: renderAccountPage,
    searchCards: searchCards,
    renderCardPage: renderCardPage,
    loadCardForm: loadCardForm,
    deleteCard: deleteCard,
    editCard: editCard,
    rename: rename
};

