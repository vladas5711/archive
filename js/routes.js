var express = require('express');
var router = express.Router();
var auth = require('./config-passport');
var bodyParser = require('body-parser');
var passport = require('passport');
var queries = require('./neo4j-queries');
var path = require('path'); 
var fs = require('fs');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({extended: false});

router.post('/login', urlencodedParser, function(req, res, next) {
    passport.authenticate('local', function(err, user) {
        if (err) {
            console.log('ERROR OF AUTHENTICATION!');
            return next(err);
        }
        if (!user) {
            return res.redirect('/');
        }
        req.logIn(user, function(err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/main');
        });
    })(req, res, next);
});

router.get('/', function(req, res) {
    res.render('signin');
});

router.get('/main', auth, function(req, res) {
    res.render('main-admin', {
        title: "Главная"
    });
});

router.get('/main-guest', function(req, res) {
    res.render('main-guest', {
        title: "Главная"
    });
});

router.get('/main/searching-cards-guest', function(req, res) {
    res.render('searching-cards-guest', {
        title: "Поиск карточек"
    });
});

router.post('/main/searching-cards-guest', function(req, res) {
    queries.searchCards(req, res);
});

router.get('/main/searching-cards-guest/edit/:id', function(req, res) {
    queries.renderCardPage(req, res);
});

router.get('/main/searching-fund-guest', function(req, res) {
    res.render('searching-fund-guest', {
        title: "Поиск фондов"
    });
});

router.post('/main/searching-fund-guest', function(req, res) {
    queries.searchFund(req, res);
});

router.get('/main/searching-inventorys-guest', function(req, res) {
    res.render('searching-inventorys-guest', {
        title: "Поиск описей"
    });
});

router.post('/main/searching-inventorys-guest', function(req, res) {
    queries.searchInventorys(req, res);
});

router.get('/main/searching-cases-guest', function(req, res) {
    res.render('searching-cases-guest', {
        title: "Поиск дел"
    });
});

router.post('/main/searching-cases-guest', function(req, res) {
    queries.searchCases(req, res);
});

router.get('/main/searching-cards', auth, function(req, res) {
    res.render('searching-cards', {
        title: "Поиск карточек"
    });
});

router.post('/main/searching-cards', auth, function(req, res) {
    queries.searchCards(req, res);
});

router.get('/main/searching-cards/edit/:id', auth, function(req, res) {
    queries.renderCardPage(req, res);
});

router.get('/main/edit-cards', auth, function(req, res) {
    res.render('edit-cards', {
        title: "Редактирование карточек"
    });
});

router.get('/main/adding-card', auth, function(req, res) {
    queries.renderAddingCardPage(req, res);
});

router.post('/main/adding-card/load-form', auth, function(req, res) {
    queries.loadForm(req, res);
});

router.post('/main/adding-card', auth, function(req, res) {
    var storage = multer.diskStorage({
        destination: 'uploads/'
    });
    var upload = multer({ storage : storage}).any();
    upload(req,res,function(err) {
        if(err) {
            console.log("Error uploading file.");
        } else {
            if (req.files.length > 0) {
                if (req.files[0].originalname.split('.')[1] == 'jpg' || req.files[0].originalname.split('.')[1] == 'png' ) {
                    var temps = '' + req.files[0].filename;
                    temps += '.' + req.files[0].originalname.split('.')[1];
                    var tmp_path = req.files[0].path;  
                    var target_path = path.join(req.files[0].destination, temps);
                    var src = fs.createReadStream(tmp_path); 
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest); 
                    src.on('end', function() { 
                        fs.unlink(tmp_path, function() {});
                    });
                    src.on('error', function(err) { 
                        fs.unlink(tmp_path, function() {});  
                        res.send('error'); 
                    });
                    req.body['image'] = target_path.replace('\\', '/');
                    queries.addCard(req, res);
                } else {
                    res.render('adding-card', {
                        title: "Добавление карточки",
                        data: `<h5 class="mb-3"><b>Невозможно закрепить такой тип файла за карточкой!</b></h5>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <a class="btn btn-primary btn-lg btn-block" href="/main/adding-card">назад</a>
                                    </div>
                                </div>`
                    });
                }
            } else {
                req.body['image'] = 'no_image';
                queries.addCard(req, res);
            };
        }
    });
});

router.get('/main/adding-fund', auth, function(req, res) {
    res.render('adding-fund', {
        title: "Добавление фондов"
    });
});

router.post('/main/adding-fund', auth, function(req, res) {
    queries.addFund(req, res);
});

router.get('/main/searching-fund', auth, function(req, res) {
    res.render('searching-fund', {
        title: "Поиск фондов"
    });
});

router.post('/main/searching-fund', auth, function(req, res) {
    queries.searchFund(req, res);
});

router.get('/main/edit-funds', auth, function(req, res) {
    res.render('edit-funds', {
        title: "Редактирование фондов"
    });
});

router.post('/main/edit-funds/edit', auth, function(req, res) {
    queries.editFund(req, res);
});

router.post('/main/edit-funds/delete', auth, function(req, res) {
    queries.deleteFund(req, res);
});

router.get('/main/adding-inventory', auth, function(req, res) {
    res.render('adding-inventory', {
        title: "Добавление описей"
    });
});

router.post('/main/adding-inventory', auth, function(req, res) {
    queries.addInventory(req, res);
});

router.get('/main/edit-inventorys', auth, function(req, res) {
    res.render('edit-inventorys', {
        title: "Редактирование описей"
    });
});

router.post('/main/edit-inventorys/edit', auth, function(req, res) {
    queries.editInventory(req, res);
});

router.post('/main/edit-inventorys/delete', auth, function(req, res) {
    queries.deleteInventory(req, res);
});

router.get('/main/adding-case', auth, function(req, res) {
    res.render('adding-case', {
        title: "Добавление дел"
    });
});

router.post('/main/adding-case', auth, function(req, res) {
    queries.addCase(req, res);
});

router.get('/main/edit-cases', auth, function(req, res) {
    res.render('edit-cases', {
        title: "Редактирование дел"
    });
});

router.post('/main/edit-cases/edit', auth, function(req, res) {
    queries.editCase(req, res);
});

router.post('/main/edit-cases/delete', auth, function(req, res) {
    queries.deleteCase(req, res);
});

router.get('/main/searching-inventorys', auth, function(req, res) {
    res.render('searching-inventorys', {
        title: "Поиск описей"
    });
});

router.post('/main/searching-inventorys', auth, function(req, res) {
    queries.searchInventorys(req, res);
});

router.get('/main/searching-cases', auth, function(req, res) {
    res.render('searching-cases', {
        title: "Поиск дел"
    });
});

router.post('/main/searching-cases', auth, function(req, res) {
    queries.searchCases(req, res);
});

router.get('/main/edit-card-types', auth, function(req, res) {
    queries.renderEditCardTypesPage(req, res);
});

router.post('/main/edit-card-types/add', auth, function(req, res) {
    queries.addCardType(req, res);
});

router.get('/main/delete-card-type', auth, function(req, res) {
    res.render('delete-card-type', {
        title: "Удаление типа"
    });
});

router.post('/main/delete-card-type', auth, function(req, res) {
    queries.deleteCardType(req, res);
});

router.get('/main/account', auth, function(req, res) {
    queries.renderAccountPage(req, res);
});

router.post('/main/account/add', auth, function(req, res) {
    queries.addAccount(req, res);
});

router.post('/main/account/delete', auth, function(req, res) {
    queries.deleteAccount(req, res);
});

router.get('/main/edit-geo', auth, function(req, res) {
    res.render('edit-geo', {
        title: "Редактор географии"
    });
});

router.post('/main/edit-geo/add-rename', auth, function(req, res) {
    queries.addRename(req, res);
});

router.post('/main/edit-geo/rename', auth, function(req, res) {
    queries.rename(req, res);
});

router.post('/main/edit-cards/load', auth, function(req, res) {
    queries.loadCardForm(req, res);
});

router.post('/main/edit-cards/delete', auth, function(req, res) {
    queries.deleteCard(req, res);
});

router.post('/main/edit-cards-result', auth, function(req, res) {  
    var storage = multer.diskStorage({
        destination: 'uploads/'
    });
    var upload = multer({ storage : storage}).any();
    upload(req,res,function(err) {
        if(err) {
            console.log("Error uploading file.");
        } else {
            if (req.files.length > 0) {
                if (req.files[0].originalname.split('.')[1] == 'jpg' || req.files[0].originalname.split('.')[1] == 'png' ) {
                    var temps = '' + req.files[0].filename;
                    temps += '.' + req.files[0].originalname.split('.')[1];
                    var tmp_path = req.files[0].path;  
                    var target_path = path.join(req.files[0].destination, temps);
                    var src = fs.createReadStream(tmp_path); 
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest); 
                    src.on('end', function() { 
                        fs.unlink(tmp_path, function() {});
                    });
                    src.on('error', function(err) { 
                        fs.unlink(tmp_path, function() {});  
                        res.send('error'); 
                    });
                    req.body['image'] = target_path.replace('\\', '/');
                    queries.editCard(req, res);
                } else {
                    res.render('edit-cards-result', {
                        title: "Результат",
                        data: `<h5 class="mb-3"><b>Невозможно закрепить такой тип файла за карточкой!</b></h5>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <a class="btn btn-primary btn-lg btn-block" href="/main/edit-cards">назад</a>
                                    </div>
                                </div>`
                    });
                }
            } else {
                req.body['image'] = 'no_image';
                queries.editCard(req, res);
            };
        }
    });
});

router.get('/logout', function(req, res, next) {
    req.logout();
    res.redirect('/');
});

module.exports = router;