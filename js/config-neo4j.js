var neo4j = require('neo4j-driver').v1;

var driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic('neo4j', '12345'));
var neo4j_session = driver.session();

module.exports = neo4j_session;