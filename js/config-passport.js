var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var queries = require('./neo4j-queries');
var bcrypt = require('bcrypt');

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    queries.findById(id, function(admin) {
        done(null, admin);
    })
});

passport.use(new LocalStrategy(
    function(username, password, done) {
        queries.findAdmin(username, password, function(admin) {
            bcrypt.compare(password, admin.password, function(err, res) {
                if (res == true) {
                    return done(null, admin);
                } else {
                    return done(null, false);
                }
            });
        })
    })
);

function auth(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/');
    }
}

module.exports = auth;


