var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var path = require('path'); 
var configs = require(path.join(__dirname, '/configs.json'));

var app = express();
var port = configs.port;

app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'pages'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(cookieParser());
app.use('/js', express.static(__dirname + '/js')); 
app.use('/css', express.static(__dirname + '/css'));
app.use('/images', express.static(__dirname + '/images'));  
app.use('/uploads', express.static(__dirname + '/uploads')); 
app.use(session({
    //store: "TO DO: STORE",
    resave: false,
    saveUninitialized: false,
    secret: 'stepanenko-app'
}));
require(path.join(__dirname, '/js/config-passport.js'));
app.use(passport.initialize());
app.use(passport.session());

app.use(require('./js/routes'));

app.listen(port, function() {
    console.log('Server was started on port ' + port);
})